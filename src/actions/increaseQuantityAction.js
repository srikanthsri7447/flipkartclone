import { Increase_Quantity } from "../allTypes"; 

const increaseQuantity=(event)=>dispatch=>{
    const id = event.target.id
    dispatch({
        type:Increase_Quantity,
        payload:Number(id)
    })
}

export default increaseQuantity;