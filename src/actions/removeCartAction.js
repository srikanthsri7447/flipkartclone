import { Remove_Cart } from "../allTypes";

const removeCartAction =(event)=>dispatch=>{
    const id = event.target.id
    dispatch({
        type:Remove_Cart,
        payload:id
    })
}

export default removeCartAction;