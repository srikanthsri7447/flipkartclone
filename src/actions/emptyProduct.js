import { Empty_Product } from "../allTypes";

const emptyProduct=()=>dispatch=>{
    dispatch({
        type:Empty_Product,
        payload:[]
    })
}

export default emptyProduct;