import axios from "axios"
import { All_Products } from "../allTypes"

const getAllProducts =()=>dispatch=>{
    axios.get('https://fakestoreapi.com/products')
    .then((res)=>res.data)
    .then((data)=>dispatch({
        type:All_Products,
        payload:data
    }))
}

export default getAllProducts;