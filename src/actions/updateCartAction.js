import { Add_Cart, Selected } from "../allTypes";
import axios from "axios";

export const addCartAction=(event)=>dispatch=>{
    console.log('click')
    const id = event.target.id
    axios.get(`https://fakestoreapi.com/products/${id}`)
    .then((res)=>res.data)
    .then((data)=>dispatch({
        type:Add_Cart,
        payload:data
    }))
}

export const addCart=(event)=>dispatch=>{
    dispatch({
        type:Selected,
        payload:event.target.id
    })
}