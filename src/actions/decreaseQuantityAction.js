import { Decrease_Quantity } from "../allTypes";

const decreaseQuantity=(event)=>dispatch=>{
    const id = event.target.id
    console.log(id)
    dispatch({
        type:Decrease_Quantity,
        payload:Number(id)
    })
}

export default decreaseQuantity;