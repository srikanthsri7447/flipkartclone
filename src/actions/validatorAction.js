import { Validate_Email_Or_Phonenumber, Submit_Address, Validate_Name ,Validate_Mobile, Validate_Pincode, Validate_Locality, Validate_Address, Validate_City, Validate_State, Validate_Card, Validate_Cvv, Verify_Payment, Validate_Expiry} from "../allTypes";
import { Validate_Password } from "../allTypes";
import { On_Submit } from "../allTypes";


// Login Form validation

export const validateEmailorPhone = (event) => dispatch => {
    dispatch({
        type: Validate_Email_Or_Phonenumber,
        payload: event
    })
}

export const validatePassword = (event) => dispatch => {
    dispatch({
        type: Validate_Password,
        payload: event
    })
}

export const formSubmission = (event) => dispatch => {
    event.preventDefault()
    dispatch({
        type: On_Submit
    })
}

export const addressSubmission = (event) => dispatch => {
    console.log('add sub')
    event.preventDefault()
    dispatch({
        type: Submit_Address
    })
}

export const clickPay =(event)=>dispatch=>{
    event.preventDefault()
    dispatch({
        type:Verify_Payment
    })
}


export const validateInput = (event) => dispatch => {
    const name = event.target.name
    const value = event.target.value

    switch (name) {
        case ('name'):
            dispatch({
                type: Validate_Name,
                payload: value
            })
            break
        case('mobile'):
        dispatch({
            type:Validate_Mobile,
            payload:value
        })
        break
        case('pincode'):
        dispatch({
            type:Validate_Pincode,
            payload:value
        })
        break
        case('locality'):
        dispatch({
            type:Validate_Locality,
            payload:value
        })
        break
        case('address'):
        dispatch({
            type:Validate_Address,
            payload:value
        })
        break
        case('city'):
        dispatch({
            type:Validate_City,
            payload:value
        })
        break
        case('state'):
        dispatch({
            type:Validate_State,
            payload:value
        })
        break
        case('card'):
        dispatch({
            type:Validate_Card,
            payload:value
        })
        break
        case('mm'):
        dispatch({
            type:Validate_Expiry,
            payload:value
        })
        break
        case('yyyy'):
        dispatch({
            type:Validate_Expiry,
            payload:value
        })
        break
        case('cvv'):
        dispatch({
            type:Validate_Cvv,
            payload:value
        })
        break
        default:
            return null
    }

}
