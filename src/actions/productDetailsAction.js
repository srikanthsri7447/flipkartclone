import axios from "axios"
import { Each_Product } from "../allTypes"

const productDetailsAction =(value)=>dispatch=>{

    axios.get(`https://fakestoreapi.com/products/${value}`)
    .then((res)=>res.data)
    .then((data)=>dispatch({
        type:Each_Product,
        payload:data
    }))

}

export default productDetailsAction;