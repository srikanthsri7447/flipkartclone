import { Submit_Address, Validate_Address, Validate_Card, Validate_City, Validate_Cvv, Validate_Expiry, Validate_Locality, Validate_Mobile, Validate_Name, Validate_Pincode, Validate_State, Verify_Payment } from "../allTypes";
import validator from 'validator';

const addressValidationReducer = (state = {
    name: '',
    mobile: '',
    pincode: '',
    Locality: '',
    address: '',
    city: '',
    state: '',
    isNameValid: false,
    isMobileValid: false,
    isLocalityValid: false,
    isPincodeValid: false,
    isAddressValid: false,
    isCityValid: false,
    isStateValid: false,
    nameErr: '',
    mobileErr: '',
    pincodeErr: '',
    LocalityErr: '',
    addressErr: '',
    cityErr: '',
    stateErr: '',
    mm:'',
    yyyy:'',
    isAddressSubmitted: false,
    isCardNumberValid: false,
    cardErr: '',
    isExpiryValid: false,
    expiryErr: '',
    isCvvValid: false,
    cvvErr: ''
}, action) => {
    console.log('add red')

    switch (action.type) {
        case (Submit_Address):
            if ((state.isNameValid &&
                state.isAddressValid &&
                state.isMobileValid &&
                state.isCityValid &&
                state.isLocalityValid &&
                state.isStateValid &&
                state.isPincodeValid)) {
                return state = { ...state, isAddressSubmitted: true }

            }
            else {
                state.nameErr = '*Require Name'
                state.cityErr = '*Require City'
                state.mobileErr = '*Require Mobile Number'
                state.pincodeErr = '*Require Pincode'
                state.LocalityErr = '*Require Locality'
                state.addressErr = '*Require Address'
                state.cityErr = '*Require City'
                state.stateErr = '*Require State'
                return state = { ...state }
            }


        case (Validate_Name):
            if (!action.payload || action.payload.length < 3 || (!validator.isAlpha(action.payload))) {
                state = { ...state, nameErr: '*Enter Valid Name' }
                return state
            }
            else {
                state = { ...state, name: action.payload, isNameValid: true, nameErr: '' }
                return state
            }
        case (Validate_Mobile):
            if (!action.payload || action.payload.length < 10 || (!validator.isMobilePhone(action.payload, 'en-IN'))) {
                state = { ...state, mobileErr: '*Enter Valid Mobile Number' }
                return state
            }
            else {
                state = { ...state, mobile: action.payload, isMobileValid: true, mobileErr: '' }
                return state
            }
        case (Validate_Pincode):
            if (!action.payload || action.payload.length < 6 || (!validator.isPostalCode(action.payload, 'IN'))) {
                state = { ...state, pincodeErr: '*Enter Valid Pincode' }
                return state
            }
            else {
                state = { ...state, pincode: action.payload, isPincodeValid: true, pincodeErr: '' }
                return state
            }
        case (Validate_Locality):
            if (!action.payload || action.payload.length < 3 || (!validator.isAlpha(action.payload))) {
                state = { ...state, LocalityErr: '*Enter Valid Locality' }
                return state
            }
            else {
                state = { ...state, Locality: action.payload, isLocalityValid: true, LocalityErr: '' }
                return state
            }
        case (Validate_Address):
            if (!action.payload || action.payload.length < 3) {
                state = { ...state, addressErr: '*Enter Valid Address' }
                return state
            }
            else {
                state = { ...state, address: action.payload, isAddressValid: true, addressErr: '' }
                return state
            }
        case (Validate_City):
            if (!action.payload || action.payload.length < 3 || (!validator.isAlpha(action.payload))) {
                state = { ...state, cityErr: '*Enter Valid City' }
                return state
            }
            else {
                state = { ...state, city: action.payload, isCityValid: true, cityErr: '' }
                return state
            }
        case (Validate_State):
            if (action.payload === '*SelectState') {
                state = { ...state, stateErr: '*Enter Valid City' }
                return state
            }
            else {
                state = { ...state, state: action.payload, isStateValid: true, stateErr: '' }
                return state
            }
        case (Verify_Payment):
            if (state.isNameValid &&
                state.isAddressValid &&
                state.isMobileValid &&
                state.isCityValid &&
                state.isLocalityValid &&
                state.isStateValid &&
                state.isPincodeValid &&
                state.isCardNumberValid && 
                state.isExpiryValid && 
                state.isCvvValid) {
                state = { ...state, isPaymentSuccessful: true }
                return state
            }
            else{
                state.cardErr = '*Enter Card Details'
                state.expiryErr = '*Enter Expiry Date'
                state.cvvErr = '*Enter Cvv Number'
                return state = {...state}
            }
        case(Validate_Card):
        if(!action.payload || action.payload.length<16 || (!validator.isCreditCard(action.payload))){
            state.cardErr = '*Enter Valid Card Number'
            return state = {...state}
        }
        else{

            state = {...state, isCardNumberValid :true,cardErr:''}
            return state
          
        }
        case(Validate_Expiry):
        if(action.payload === 'MM' || action.payload ==='YYYY'){
            state.expiryErr = '*Enter Valid Card Expiry'
            return state = {...state}
        }
        else{
            state = {...state, isExpiryValid :true, expiryErr:''}

            return state
        }
        case(Validate_Cvv):
        if(!action.payload || action.payload.length !== 3 || (!validator.isNumeric(action.payload))){
            state.cvvErr = '*Enter Valid Card Expiry'
            return state = {...state}
        }
        else{
            state = {...state, isCvvValid :true, cvvErr:''}

            return state
        }

        default:
            return state
    }
}

export default addressValidationReducer;