import { combineReducers } from "redux";
import productReducer from "./productReducer";
import productsReducer from "./productsReducer";
import updateCartReducer from "./updateCartReducer";
import validateEmailPhone from "./validatorReducer";
import addressReducer from "./upateAndValidateAddressReducer";
import ValidatePaymentReducer from "./ValidatePaymentDetailsReducer";
import addressValidationReducer from "./addressValidationReducer";


const allReducers = combineReducers({
    products : productsReducer,
    product:productReducer,
    cart:updateCartReducer,
    validator:validateEmailPhone,
    address:addressReducer,
    payment : ValidatePaymentReducer,
    deliveryAddress:addressValidationReducer

})

export default allReducers;