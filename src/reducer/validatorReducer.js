import { Validate_Email_Or_Phonenumber } from "../allTypes";
import validator from 'validator';
import { Validate_Password } from "../allTypes";
import { On_Submit } from "../allTypes";

const validateEmailPhone = (state = { isValidEmailOrPhone: false, emailOrPhone: '', passwordInput: '', isValidPassword: false, emailErrMessage: '', passwordErrMessage: '', isLogged: false }, action) => {

    switch (action.type) {

        case (Validate_Email_Or_Phonenumber):
            const value = action.payload.target.value
            if (!value) {
                state = { ...state, emailErrMessage: 'Enter your Email or Phone Number' }
            }
            else if (validator.isEmail(value) || validator.isMobilePhone(value, 'en-IN')) {
                state = {
                    ...state,
                    isValidEmailOrPhone: true,
                    emailOrPhone: value,
                    emailErrMessage: ''
                }
            }
            else {
                state = { ...state, emailErrMessage: 'Enter your Email or Phone Number' }
            }

            return state


        case (Validate_Password):
            const pass = action.payload.target.value

            if (!pass) {
                state = { ...state, passwordErrMessage: 'Enter a valid Password' }
            }
            else if (validator.isStrongPassword(pass)) {
                state = {
                    ...state,
                    isValidPassword: true,
                    passwordInput: pass,
                    passwordErrMessage: ''
                }

            }
            else {
                state = { ...state, passwordErrMessage: 'Enter a valid Password' }
            }

            return state

        case (On_Submit):

            if (!state.emailOrPhone) {
                state = { ...state, emailErrMessage: 'Enter your Email or Phone Number' }

            }
            if (!state.isValidEmailOrPhone) {
                state = { ...state, emailErrMessage: 'Enter your Email or Phone Number' }

            }
            if ((state.passwordInput = "")) {
                state = { ...state, passwordErrMessage: 'Enter a valid Password' }

            }
            if (!state.isValidPassword) {
                state = { ...state, passwordErrMessage: 'Enter a valid Password' }


            }
            if (state.isValidEmailOrPhone && state.isValidPassword) {
                state = { ...state, isLogged: true }

            }

            return state
        case ('submitAddress'):
            return state

        default:
            return state
    }


}


export default validateEmailPhone;