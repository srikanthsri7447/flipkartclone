import { Add_Cart } from "../allTypes";
import { Remove_Cart } from "../allTypes";
import { Increase_Quantity } from "../allTypes";
import { Decrease_Quantity } from "../allTypes";

const updateCartReducer = (state = [], action) => {
    console.log('type is', action.type, 'datails' ,action.payload)
    switch (action.type) {
        case (Add_Cart):
            if (!state) {
                action.payload.quantity = 1
                return state = [...state, action.payload]
            }
            else {

                let check = !!state.find(({ id }) => Number(id) === Number(action.payload.id))
                if (check) {
                    return state
                }
                else {
                    action.payload.quantity = 1
                    return state = [...state, action.payload]
                }
            }
        case (Remove_Cart):

            const updatedState = state.filter((eachProduct) => Number(eachProduct.id) !== Number(action.payload))
            return state = [...updatedState]

        case (Increase_Quantity):

            const addQuantity = state.map((eachProduct) => {

                if (Number(eachProduct.id) === action.payload) {
                    eachProduct.quantity += 1
                    return eachProduct
                }
                else {
                    return eachProduct
                }
            })
            return state = addQuantity

        case (Decrease_Quantity):
            const decreaseQuantity = state.map((eachProduct) => {

                if (Number(eachProduct.id) === action.payload) {
                    if (eachProduct.quantity > 0) {
                        eachProduct.quantity -= 1
                        return eachProduct
                    }
                    else{
                        return eachProduct
                    }

                }
                else {
                    return eachProduct
                }
            })
            return state = decreaseQuantity


        default:
            return state


    }

}

export default updateCartReducer;