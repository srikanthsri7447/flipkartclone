import './App.css';
import { connect } from 'react-redux';
import React, { Component } from 'react'
import getAllProducts from './actions/getAllProductsAction';
import { BrowserRouter } from 'react-router-dom';
import Header from './components/Header';
import Home from './components/Home';
import { Switch } from 'react-router-dom';
import { Route } from 'react-router-dom';
import Products from './components/Products';
import ElectronicsPage from './components/ElectronicsPage';
import ClothesPage from './components/ClothesPage'
import JeweleryPage from './components/JeweleryPage';
import ProductDetails from './components/ProductDetails';
import CartPage from './components/CartPage';
import * as Loader from 'react-loader-spinner'
import CheckOut from './components/checkOut';
import Successfull from './components/successfull';

class App extends Component {

  componentDidMount() {
    this.props.getAllProducts()
  }

  render() {

    const HideHeader = window.location.pathname==='/successfull' ? null : <Header />

    return !this.props.local_variable.products ? <Loader.Audio color='#2874f0' /> :
  
      <div>
        <BrowserRouter>
          <div className='fixed-top'>
            {HideHeader}
          </div>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/products' component={Products}  />
            <Route exact path='/electronics' component={ElectronicsPage} />
            <Route exact path='/clothes' component={ClothesPage} />
            <Route exact path='/jewelery' component={JeweleryPage}/>
            <Route exact path='/product/:id' component={ProductDetails}/>
            <Route exact path='/viewcart' component={CartPage} />
            <Route exact path='/checkOut' component={CheckOut} />
            <Route exact path='/successfull' component={Successfull} />
          </Switch>
        </BrowserRouter>
      </div>
    
  
}

}


const mapStateToProps = state => ({
  local_variable: state
})

export default connect(mapStateToProps, { getAllProducts })(App);
