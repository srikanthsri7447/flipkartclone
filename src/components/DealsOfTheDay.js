import React from 'react'
import { Link } from 'react-router-dom'

function DealsOfTheDay() {
    return (
        <>
            <div className='container-fluid d-none d-lg-block'>
                <div className='row'>
                    <div className='col-9'>
                        <div className='d-flex justify-content-between'>
                            <h4>Deals of the Day</h4>
                            <Link to='/products'> <button className='btn btn-primary'>VIEW ALL</button> </Link>
                        </div>
                        <hr className='w-100' />
                        <div>
                            <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
                                <div className="carousel-inner">

                                    <div className="carousel-item active">
                                        <div className='d-flex flex-wrap'>
                                            <Link to='/products' className='d-flex flex-column align-items-center justify-content-around p-2 w-25 text-center'>
                                                <img src="https://rukminim2.flixcart.com/flap/150/150/image/80a29fb2576bebff.jpg?q=70" className="d-block" alt="..." />
                                                <span>Never Before Prices!</span> <br />
                                                <span className='text-success'>From ₹79</span>
                                            </Link>

                                            <Link to='/products' className='d-flex flex-column align-items-center justify-content-around p-2 w-25 text-center'>
                                                <img src="https://rukminim2.flixcart.com/image/150/150/kl175ow0/watch/k/w/a/anlg-428-brown-brwn-analogue-original-imagy8tnbzftnw3a.jpeg?q=70" className="d-block" alt="..." />
                                                <span>Titan, Fastrack, Casio..</span> <br />
                                                <span className='text-success'>Upto 75%+Extra 10%Off</span>
                                            </Link>

                                            <Link to='/products' className='d-flex flex-column align-items-center justify-content-around p-2 w-25 text-center'>
                                                <img src="https://rukminim2.flixcart.com/image/150/150/jzrb53k0/cookware-set/f/s/t/8901365367581-36796-prestige-original-imafjp5dygmtj9ae.jpeg?q=70" className="d-block" alt="..." />
                                                <span>Kitchen and Dinning Essentials</span> <br />
                                                <span className='text-success'>From ₹199</span>
                                            </Link>

                                            <Link to='/products' className='d-flex flex-column align-items-center justify-content-around p-2 w-25 text-center'>
                                                <img src="https://rukminim2.flixcart.com/image/150/150/jyj0how0/ball/p/z/a/430-5-21-9-attacker-1-na-football-vector-x-original-imafgqtfhcbtqkzv.jpeg?q=70" className="d-block" alt="..." />
                                                <span>Football, Cricket, Badminton...</span> <br />
                                                <span className='text-success'>Upto 70% + 10% Off</span>
                                            </Link>
                                        </div>
                                    </div>

                                    <div className="carousel-item">
                                        <div className='d-flex flex-wrap'>

                                            <div className='d-flex flex-column align-items-center justify-content-around p-2 w-25 text-center'>
                                                <img src="https://rukminim2.flixcart.com/flap/150/150/image/43d9c318e1076cb2.jpeg?q=70" className="d-block" alt="..." />
                                                <span>Never Before Prices!</span> <br />
                                                <span className='text-success'>From ₹79</span>
                                            </div>

                                            <div className='d-flex flex-column align-items-center justify-content-around p-2 w-25 text-center'>
                                                <img src="https://rukminim2.flixcart.com/image/150/150/kjuby4w0/curtain/n/w/w/cotton-maroon-color-three-in-one-ultimate-ruffle-rod-curtain-set-original-imafzbx9xggsts5z.jpeg?q=70" className="d-block" alt="..." />
                                                <span>Titan, Fastrack, Casio..</span> <br />
                                                <span className='text-success'>Upto 75%+Extra 10%Off</span>
                                            </div>

                                            <div className='d-flex flex-column align-items-center justify-content-around p-2 w-25 text-center'>
                                                <img src="https://rukminim2.flixcart.com/image/150/150/kjym9ow0/top/t/a/h/s-fg-479-farhan-garment-original-imafzeujuvt6nw7f.jpeg?q=70" className="d-block" alt="..." />
                                                <span>Kitchen and Dinning Essentials</span> <br />
                                                <span className='text-success'>From ₹199</span>
                                            </div>

                                            <div className='d-flex flex-column align-items-center justify-content-around p-2 w-25 text-center'>
                                                <img src="https://rukminim2.flixcart.com/image/150/150/kv2pk7k0/ethnic-set/5/o/1/l-vv-9081-green-l-vredevogel-original-imag823rfxapk8xe.jpeg?q=70" className="d-block" alt="..." />
                                                <span>Football, Cricket, Badminton...</span> <br />
                                                <span className='text-success'>Upto 70% + 10% Off</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span className="visually-hidden">Previous</span>
                                </button>
                                <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span className="visually-hidden">Next</span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div className='col-3'>
                        <img src='https://rukminim2.flixcart.com/flap/461/353/image/209cf9cd9cfd432b.jpg?q=70' alt='mobile' />
                    </div>

                </div>

            </div>

            <div className='d-flex d-lg-none flex-column w-100 p-3'>
                <div className='w-100'>
                    <img className='w-100' src='https://rukminim1.flixcart.com/flap/1250/300/image/cb65ccf0fa22a49d.jpg?q=90' alt='...' />
                </div>
                <div className='d-flex w-100'>

                    <Link to="/products">
                    <img style={{ width: '33%' }} src='https://rukminim1.flixcart.com/flap/450/550/image/00c56da5993342dd.jpg?q=90' alt='...' />
                    </Link>

                    <Link to="/products">
                    <img style={{ width: '33%' }} src='https://rukminim1.flixcart.com/flap/450/550/image/adec58503f043794.jpg?q=90' alt='...' />
                    </Link>

                    <Link to="/products">
                    <img style={{ width: '33%' }} src='https://rukminim1.flixcart.com/flap/450/550/image/5ebfca659385f413.jpg?q=90' alt='...' />
                    </Link>

                </div>

            </div>
        </>
    )
}

export default DealsOfTheDay