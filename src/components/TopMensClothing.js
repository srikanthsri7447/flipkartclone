import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import '../App.css'

class TopMensClothing extends Component {

    render() {
        const products = this.props.local_variable.products
        const mensCloths = products.filter((eachProduct) => eachProduct.category === `men's clothing`)
        return (
            <div className='container-fluid'>
                <div className='row'>

                    <div className='col col-md-9'>

                        <div className='d-flex justify-content-between'>
                            <h4>Men's Clothing</h4>
                            <Link to='/clothes'>
                            <button className='btn btn-primary'>VIEW ALL</button>
                            </Link>
                        </div>

                        <hr className='w-100' />

                        <div className='d-flex overflow-auto example'>
                            {mensCloths.map((eachProduct) => {

                                const { id, title, price, image } = eachProduct

                                return (

                                    <Link to={`/product/${id}`} key={id} className='d-flex flex-column align-items-center m-3 text-center' >
                                        <div className='imgSize'>
                                            <img src={image} alt='product' className='w-75' style={{ maxHeight: "100px" }} /> <br />
                                        </div>
                                        <span className='fw-bold textControl'>{title}</span> <br />
                                        <span className='text-success'>Price: ₹{price}</span>
                                    </Link>

                                )
                            })}
                        </div>

                    </div>

                    <div className='d-none d-md-block col-md-3'>
                        <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRXvxdeROKuIL-JtTconX3xswcWmKE6TkLE6Q&usqp=CAU' alt='...' />
                    </div>

                </div>
            </div>

        )
    }
}


const mapStateToProps = state => ({
    local_variable: state
})

export default connect(mapStateToProps)(TopMensClothing)