import React from 'react'

function Carousel() {
    return (
        <>
            <div className='d-flex d-md-none p-3'>
                <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
                    <div className="carousel-inner">
                        <div className="carousel-item active">
                            <img src="https://rukminim1.flixcart.com/flap/700/350/image/6e3776eb8de8d65c.jpeg?q=90" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src="https://rukminim1.flixcart.com/flap/700/350/image/7ad844e026e6a28b.jpg?q=90" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src="https://rukminim1.flixcart.com/flap/700/350/image/6c519d2faece8a63.jpg?q=90" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src="https://rukminim1.flixcart.com/flap/700/350/image/9810afc536ef9f1d.jpeg?q=90" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src="https://rukminim1.flixcart.com/flap/700/350/image/2490fefed55bce3b.jpeg?q=90" className="d-block w-100" alt="..." />
                        </div>
                    </div>
                    <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Previous</span>
                    </button>
                    <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Next</span>
                    </button>
                </div>
            </div>

            <div className='d-none d-md-flex p-3'>
                <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
                    <div className="carousel-inner">
                        <div className="carousel-item active">
                            <img src="https://rukminim2.flixcart.com/flap/1688/280/image/ea766bcd666ed5f4.jpg?q=50" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src="https://rukminim2.flixcart.com/flap/1688/280/image/0ff1073f83cc202f.jpg?q=50" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src="https://rukminim2.flixcart.com/flap/1688/280/image/02b803d1586f6fe9.jpg?q=50" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src="https://rukminim2.flixcart.com/flap/1688/280/image/48806c6f911cae8a.jpeg?q=50" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src="https://rukminim2.flixcart.com/flap/1688/280/image/c2a53b3aef197d9f.jpg?q=50" className="d-block w-100" alt="..." />
                        </div>
                    </div>
                    <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Previous</span>
                    </button>
                    <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
        </>
    )
}

export default Carousel