import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import '../App'

class Electronics extends Component {
    render() {
        const products = this.props.local_variable.products
        const electronics = products.filter((eachProduct) => eachProduct.category === 'electronics')
        return (
            <div className='container-fluid'>
                <div className='row'>

                    <div className='col col-md-9'>

                        <div className='d-flex justify-content-between'>
                            <h4>Electronics</h4>
                            <Link to='/electronics'>
                            <button className='btn btn-primary'>VIEW ALL</button>
                            </Link>
                        </div>

                        <hr className='w-100' />

                        <div className='d-flex overflow-auto example'>
                            {electronics.map((eachProduct) => {

                                const { id, title, price, image } = eachProduct

                                return (

                                    <Link to={`/product/${id}`} key={id} className='d-flex flex-column align-items-center  m-3 text-center' >
                                        <div className='imgSize'>
                                            <img src={image} alt='product' className='w-75' style={{ maxHeight: "100px" }} /> <br />
                                        </div>
                                        <span className='fw-bold textControl'>{title}</span> <br />
                                        <span className='text-success'>Price: ₹{price}</span>
                                    </Link>

                                )
                            })}
                        </div>

                    </div>

                    <div className='d-none d-md-block col-3'>
                        <img
                            style={{width: '100%' }}
                            src='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxASEA8QDxAQDw8QFRUQEBcVDRAXFxkXFRUWFxgYGBMYHyggGBolGxUWITIhJSktLi4uFx8zODM4NygtLisBCgoKDg0OGhAQGi0lICUtLi0tLS0tLS0rLS8tLS0tLS0tKy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAJ8BPgMBIgACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAAAAgEGAwQFB//EAEUQAAIBAwICBgUJBQYGAwAAAAECAwAEERIhBTEGEyJBUWEyUnGR0RQjQoGSk6Gx8DNTcqOyFSRigsHhB0Rzg6LxFsLS/8QAGwEAAwADAQEAAAAAAAAAAAAAAAECAwQFBgf/xAA0EQACAgADBAcHBAMBAAAAAAAAAQIRAwQhEjFBUQUTYXGRsfAGIjKBocHRFBVSkkLh8WL/2gAMAwEAAhEDEQA/AKOKYUophW6cckU9KKkGmSOKYUgpxQSOKYUgpwaYiRTClFMDQIcUwpBWQUySRTgUopgaBDCpFQKAaYmOKYCoFApkmQVNRUigRIp6UVNMTQwphSU4oETU0tSKYiaalBqRQBNFFFABRRRQAUUUUAFFFFABRRRQAUUUUAU0UwpBTCsB0BxTV1uhljHPxC0gmXXFIzB11MMgRuRupBG4FWfifDY45Y4zwY20bXMUKzteTOrL16j9m2xDoCMH1qTnTouOG5K/z9ihinFW3jPRjXe8QMbW9nZWsiqzyNojUsiYUADnk/jWqnQ25E00UjwRRwIs0k7SfNdW+dDBsZOcH7J+s20S8Kd7iuinWrlwjoODNNHc3EWgWxuYGjl2cEjTICVPzY31d+6+Nc7/AOJSi3W5M9tok1iACU6pXVioWNSBqLYyN/CmpxF1M63ekcAU1WK+6HTxRzN11vLLbqJLiKOTMkakZyRjfA3rNB0GnYovX2wlkiE8UfWtrdSM+jp2x48vcaNuPMXU4m6isinFWReAyTW/DRDBGstwZsv1z5YI25kUjCBR4E0r9EJy0AglhukuHaJXjkJQOoLMGONsBWPfy9lPbjzJ6mfBemcAVIrp8V4KYUWRZ4biMuYiY3OVdc5DKwBHI71ywapNPcY5RcXTHFSBSimBqkSZBQKUU4oJJFOKQU9MQwqaUUwoESKcVjFOKBE1IqBRTEMKkVFGaBDUUuaM0ANRS5ozQA1FLmjNADUUuaM0ANRS5ozQA1FLmjNAFOFOKQUwrAdA7HRDiMdtfW1xLnq4mZn0rk4MbrsO/ciu5e33DzMLhLy9lYTx3AjeE6AOuDMBv3Lqx7BVOVSdgCT5DNZRA/qP9hqTSbspTaVHoUHTO3MvEUMslvHdSrPBMLVZSCEjUq8LA7HR4d55VrnpPbym8t7m4uJLe4ihjSc2sasrRMzfsYwOxltts93nVG6h/Uf7DU4gf1H+w1LYQ3jTfrmXDgfFuHW104je4+TvavbNK6EsXcodYiG6r2cY8a1r/jsCw8LjgLSNw+WRyWjKhh1qsh35ZC7juzVZEL+o/wBhqYQP6j/YNPZV2T1sqr1z+xd7rpBYK3ELq3eeS5vojF1TwlVjLgBiX5NyB2rJB0qtRf2NwTJ1VvarBJ82c6wrjYd47Q3qjiB/Ub7BqCCNiCD5ijq168A6+a8b+tl44T0ughThykO3UC4S4AXkszZBU95G21aEE3DbeS2ME13LocvJKo6squCFCKw3YbZ8Rkd+Kq4phT2EQ8Z6Wlp/r8Fs6U8Yt54YlWT5XdK5Zp/kohPV4OEYfSPI5xjaqvUCpq4xSVIxzm5u36+/iZFqaUVIqkQOtPWMU4oExhUioFSKZI9MKxinFADCgUUUCGBpqQVNMQwoqKmgQUUUUAFFTiooAKKKKACiiigAooooAKKKKAKeKYUophWA3x0Yg5BII5EEg++soupf3kn3jfGux0E4ZFc38EE6l4nEmoBmX0Y2YbqQeYFbPAOEQSwcZkkUlrSIvB22Gkgy8wD2vRHOhySKUG6rjf0OELqT95J943xqRdSfvJPvG+NWI9Arwah1lsZRH1yxC5zKyYzlU0/VvjetToVwNL26WGSTq48MzYZVc4UkBAwOo5G48M0bUasWxO0q3nKF1J+8k+8b40wupP3kn3jfGu1adEZpDKRNaJDHJ1CyvcgRu5wQqMB2jv4c81x+K2EtvJJDMuiWPYjmOWQQe8EYIppp6ImUZJWyRcyfvJPvG+NKWJOSSSe8nP41fOkvR2C2jfRwuWVUiBa4F5IFVyu7dVuSFO/hWKw6P2IPD7aeOZ7i/i67rVmKiMspZQI+TciDn/WpWIqst4Ek6b8/lw8rKSKYVbbPglsYDEere6xPEzfKCJBcJJohjWHO6MNycHmd6rnFII45XjicyqmEL7YZgO2Vx9HVnHkKpSTdGOeE4q2a4qRVisOHWsNlHe3cclwZ5GjgjWYxjCZDMzjfmp29n1dqw6K2rXUTBZJbW4tXuYkLsHDKY+yWXn6f4+VJ4iQLAlKqr/u4owqRVusbG2murOFuGz2ayu2syXE51KInOkagMdrScg93nWr0gggjWRU4bNbEPoSZ55yp0seSsoB1AHvqtvWqE8FpXfn+POiuimFKDTCrMJNOKSmBpiY4phSCmFBJNNUUUxDCmpRU0Ay18Jsw0cZwNwO6tqfhoxyHurJ0fHzUXsFdeWPauc97PRQ+Fdy8jzq5RGnuI1yTCsfWdkgAsWI3IwdmXlmqpN0iszyl/ky//mvQJIfnr/2R/gx+FUO16KOLWC5lsv7uyRO0gjUkh9IBAzk5yMe2suFhRxHUpKPeLEm4K0rDhXE4ZpFWJgxXc9hweRG+RvXfqu2FmkV1GEiMRaJi3ZAz2tiMcxjbPiDVirZw4KFxTTXNbnojlZ2W1KLqtPuwooorIaQUUUUAFFFFABRRRQBThUilFSK1zeO30Q4utpe29y6lo0LBwOel0ZSQO/GrOPKu7NxCwtbXia21093Lfr1ar8meMRplj2mb0iA55eA2qmQxlmCqCzHkBTTRFSVcYO2dwdiMg5GxBHeKHFXZSm4ql60o9Z41e2drxCO9mnfr4rRESAQMdZZXCnreQG7DB8AaovQjikdtfwTzkrGusOQpONaMucDfGSK516LmQ9ZOZZCo06nYthUAOMnuAcfarVjQk4UZO/8A4gk/gD7qmMFVMqeI3K0tzv14F0Nzw+W1NhLeNEkNw08UwtJCsqODqGgbowLsMn1R7K5HTHjKXd5JPGp6rComdiyoMZPhnf8ACuHjl57/AI4rJDEzZ0gtpUucdyqMk1Sik7IlNyVV6W7wL1a8W4fb/KLiG6uJjPAYFt5UmYhmAHbmbYqMfVk48Kfh/HLFjw66nneOfh8PUtCIGbrCqlVKyDYZzneqJJEy6dQK6lDrnvU5wfZsalYzhTjZiVHmRjI/8l99Lq1zK66XJenfM7HD+MKktxO0ZM0zHSQR2EkcmbSeesodIPdkmt7pVeWsioLfqmYSSMpjteqCxEDq42GBqYb77+2qyDWSNCxCqMsxCgeJOwqtnWzEsR7LiWexvraexjsrmZrV7eRpIZOpaRSr5LKVXcHLH8K7Nn0ptY7iFUZxb21q9tE5RtTuxjOrSN1HYHuNUVoGDBSO02NOCDnJwMEbHfaoeMgkHGx0nBBGd+8bHkaTw0xrGlHcuX03Hf6Pcedby1mvJ5pI4SzEu8kmNUbLsN+8jlWnxfi00zyhp5pITIzxq0jlQNR04UnbY1ywayaTjODjOM42zzxnxxWTZV2Y3OWzRIp6QGtiG1dhlQCM6fTTOTnA05znY+6qIqzGKYUNGwVWIwr5Knxxz/MUCglokUwqKBTFQ9TSg01AgphS0wNAjsy9KY7SOGMqzTSJqiJX5oHOkdawOVXURnAO1dbgHTCO4ljtXCtcsHYtAC1v2RqwJHw2rHMFedU7icDSPCFGSIG/CaOux0dRzxKKV0Ka+tOC7NzixjU255Vz5b2eih8K7l5Ha6vM997F/qasH9qwHo7HEksbypa2yaQVJBUR5BHiMd/hW5BvcXw8k/revG+HdKxDDHGLRD2V1sJ2VmITTq2XZj477bcqXDdfzS8yqvRllmVOvtAkjSsY5S3zekKWZMKo225k+Z9pNt4f0diZcz3ARjyVVJx7WI/KvPuEdI5L29Ekg0iNSEGrOlS+cA4Gwq4zdJVRiuiV8Y3WNSNxnY5rLBuK2Y+tF2fY1sTBjOdz10+7O0ejln33bj/t/wC1K3ALIf8AOP8Ac/7VzbfiyuFbJUMDs2kEbgbj6qy3zwu7styqgnYDrMDAPLbbkPte2rU5cZEfp8LhBevmbD8GsR/zjfdf7Vx+I2saH5qZZVP+Fgw9oNbF5ex6NKPEcKozpbUSDgkEjbYA7+PjXJecEgagefIeRpxnK9WRi5bC2G0qpdo1FGaM1snKCijNGaAKbTikFMK1zeM9rNocNgMMMpGcZDqUYZ7tmO9bsnFWMfVBSI9JQDrCdvmsE7AMw6rnj6ZrW4c6rIC5wumRScE41Ruo2G/NhXRtEterCkxtKA7FiJwpwkpGRsSM9XywTvt4g1daMiPjTBVUoCFES+kwPzTZznuLYUH+EVnfpAx1YQgtuD1vI9WI87KO1tnPLy76V5rQ8lAADKOzJqxrlIwc414MW7ZAAOayTC01SoNAbdUYddoB1OF5nnjRkk42yM8iqQ/e5mNuOsfogA51DWcNkSjtDvGZc48VFZLni460SR6nwhQ62P0pGc+0YOMeFHC72BFiEm7RStICEyN9K48xjtf5Md9FvNagYIGGVQ2VlOwaJmBwfT7MuCuBuKfyC3zGTjxGR1S7gKBrOkAMxUFcbgagO7lRHxtgV7LMA2rDTM2TqiIycb46vA2+lTG5tSmlgSVTCbuMEqNW4B31YxnbY1iuBbmSNk0iHIDr84Tsck7nJGMeG+eeMkVciW5czLFxxwCCudiAdWSBpQYGQez2M479VLNxIM8UhU64imO2CCFYt4DG+B8TTvJZ5IC7YyD876W23MZTn3ZxUNcQGSRmVWU6Au0gAAiYHABB9IJz7qarkJt8WjLDxtwANIz2dwQPRx3Y25d2MEk99NBxYk4fUFIAyHJYYVxlT3El+dYxJa4zpGcZxiX08Hvz+z5DHPPlWC+aEiPqV0nHb3bngcwfPVyp0uRMnJa2b1xxwtqCrgEtjtY2L6u4ZBI2O+9NLxst9BgPKYhhkEZDafS3G+O7zriinFVsonrJczrHjBOsMp0yDSQr7AEuTgEf4ycbbgZrTjuCoIXbtrIpzuCgYDbv9L8K1xTCnSJcmzopxLDSnRhZFCBQ2ygKVA5bgA+VabYydOdOTjOM47s+dYnlVcamRc8tRA/OmicH0SD7GB/Ki1ENmcldPwZkFSK1mvYhs0sQPlMnxpku4zykQ+x1P+tVTJcJLSn4GwKatc3UY3Lxgeci1H9owfvofvk+NG8WxLk/A2qK1BxKD9/B98nxrNBcxvnq3R8c9MinGeXL2GgTi6ujrcK4lbI4Mk0KOuYzqnjUgZBIIJ23Aqy23FOH6kf5Vaa0yVPy2HvUqdtXgTVU4dclWxn6R7/Orfw+9OOZ95rnPed+HwruXkc6y4xa/Kb4/KbfSwTSflMQB7b8jq328KpfEOjfB19B4tuWOIZ/+9Xnh05+U3+59GLv/wAb1p8VkO+599IopnAOH2kdzF1DJqdlQgT68jPhk166eHRorM2FRQWYnAAA3JJ8AK84sT/ebf8A6qfnXqPG11JFF3TTIjfwrmVh7CIyp8mNUtxPF9y+5wWikcBxHHbxH0Wl1NI38MKctt9yT4gVoQGN5HjS9gLoxVlexYAEEggsr5zkHurLxPpKoldYV1XJdok6xMRIiM6bH6ZJjZsD1vLFbUvBzcmNmmkaZF7TAaPMnQTjGd+41S7QfYI9u6MiTxoBJtHJG2qNjgnGSAVJAOOY251p8atQsTnHLH500E04S+tJwuuGIXluy5xlWz38u0EOOW58a3Oka/MSH2f1Cqi/eRjxfgl3MpdFFFbpwgooooApwphSimFa5ujCmBqbf0l5HY/rNbWoeqK1cxm1gySavSzv9Eez+J0lgyxY4ijUtmmm+CfB9prBv1imB/WK2NXkP19dZbeCSTOiMvpGWwhbA8TjkKwfucf4PxR1H7F4qVvHj/V/k1Qf1ipB/WK2jby/uzyVvQfkeR9hyMHvrOOGXJcoLdzIoBZeqfIB5EjGQKf7nH+L8SX7GYi35iPg/wAmgD+sU2f1is8cEp3WPIyF2jJ3PIbePhTC0nIBELkFtIPVPufVBxu3lR+5x/i/EH7FYq35iP8AV/kwA/rFTn9Yp2ilA1MhC4DE6Wxhs4OfDY4PfisXXHwH2aP3OP8AF+IL2Jxnux4/1f5MoP6xTitfrW8vs1nQ8u768jkO+tjL5xY0tlKtLOX0x7N4nR2AsaWIpK0qSa3329g4phSinrePNMkUwpRUigRKQsx7OMqhbby0fGrt0dgzGjDIJAzVR4c5EmRvhHyPHeOrd0PugYtJ57r7q8P7UY2K5uF+6qpd618j6B0NKS6HjXCUvq2dufgFrcDMsI6zkHCgN76pb8OETSxPHKGViqkNsQDsfHl3VbuCcVWRjCDvICoI7iNfL6lNcDjvFCqywPra4Qn0kY9g7qwbbJA2xnfBqeh49JqHU5aUtf8AHalXutXot2jXfdCnjYWVxG801pVbVWrvVP5Ffm4NcSMURTpIzkkjA79ztmtC9/4fXRwykZLacdYAd+Tc+Xlz8qzdGulMxYdkY5emQR7Qc/nXofC7t5ipKBPAk5x7B317WOb6cyMFtxjsrind/WzQznSWXzGNsqMdNN1P7HmUvRySzz1qtqYYJbdD7CMitbo3Ay9YTsGA0jwCls17D0kMbRNChQs3Ikqdzzrzc8OaCQh2DZ5EPq5E1eX6enmMJ4GY0xJNc/eSttpcFo0ZelI4WL0Y8TZUdiqS/wDUopv52/nqYY5cO38R/M1YuHXm3OqjNKFZySAA7ZJPme+ti14tEOcsY/7i1ovecePwruRZ+G3H94vj4rF/U9Y+IS5riWPF4RLcsZowHWML84u+C2cb01xxeA8povvU+NIoyWLf3iD/AKi/nXp95LlVbmY2WQfVkN9ektXlFhJmeEg5GsEV6THdVcVaI4/JFc4vbNZ3UkqsscFy5mido9SB3JZ4yw9AlmZh3HO/fUt0jgVlecIxzgaJkRSTjGDksfYM1Yg4wVB7DbMhVXQ+RRgdvIYFaVtwi3jfXFBZwv68XD4VcexjnHuquADycPbVPJI3zl6FhVRnsR4JbGd+WOfqeea3eI2BuFaJSFLd5BIG4Pd7KI41UlslnOxZmy3wA8gAK5/SS7ljsrqS3dknDQJE3WYAMkix8iCv0u8UrrUeynoypXlq8UjRuMMpxy5+BHkawV1uk0zNc5JXQUAjGGLjSBnUxPa3D9wxtXJrdg9qKZw8bD2JuKCiiiqMRTKYUtMK1zeZmtv2i+w/ka2q1rV1Eis4YoM6tJGdweWdq6vyqz9S5/l1zc5l8TEmnBXp92ez9mumcpkstPDx5U3NtaN6bMVwXNM06sPRTi0UHWLK0iqzI+EXc6NW6uHVo37WxyRucjlXL+U2fqXP8uj5RZ+pc+6OtVZPHW6P1R3Mf2i6KxoOEsR0+UZXz5FrtOlluhQmMnq47NCdJyxgaMsMltP0SQQBuFrEOlEMcWlDLPJGo0vKGUkiYy9rQ+QAD625z3VWuvtPUuf5fxqeutPVufdH8ar9PmuXkaX7n0J/OW9PdLhu4Xw5/arBwHpTGhkNwpczXMdxIFUcl1sSu+za2QgeAIrM3SuEYdOs1KsaKhQaB1dwJus1as6iByxnLHeq11lp6tz/AC/jR1lp6tz/AC/jQstmapR8ip9KdCSm5ub1r/GVadlHe6S9IoJ7cwwq4IkVU1KoxDCr9WDj6WZnP1CqjXR1Wfhce+P402bPwuP5dKWUzEnbj5GzlvaDojLw2IYjrtjJvyOZWUHb6h/Stb2bT1bn3R/Gta4KEnq9ejkNWM8gDy27q2sllsXDxNqSpV2HF9pemslncosLAnctpPc1pTXFdqEFTmoFMBXVPCkgmmBpQKYUCM9lOFZmY4+bcD2kpisfA+OiGN9DlpXkkZBg4HcDv7653FTgDnjcbKT4eFc2CdVZciTV3DQ+Tjw28642cyEMxitU25ON7kqSre+d6nvegMzhQyUYYsopXJu3q9eXd2cezW8dDLx1vY3ZiwDE49oAJx/mNav/ABE4+TfSJCjJJASrMQp1I+G2Hs5E9zGtfoJfp8pklunWBI0JjRo2AYsw2Bx3BfxriccuzcXl5NpKiRiUPVvjAwo/Ct/9KsHOXCXwq7Wit0q7dN5qZ/NZfP4zde5SjrWure7v+vAg3q/KBJChQkDXuo5nBbA7t6sPDuksn7JWRW5E6/DbbxFVG3jLLqJPWQnY6SSVyBgY54/LNehcP44F4bbaU6ohShUjB7Lld/E8q6bxpY+LJN2704xS37uO/nvtO6NCOJDKYUVGO1olq1fFb6vTdrwSa3maLiYjBkaVWkGwBcFvbo/XOuNcXjStk4GFIAGdt/HvO1b8nCDNAxVRpXrZHZokyMqZFYMQxYHYZyPxNcO3YMTpPLHNHXmW8a4koSnm8PFm7fvcPhVPS+1v8HU6RzOFidHZiFJNdXTvVpyTenZ8/kcvjLdiUeL4P260OC8NjlkmVy6iPGCvtI328vKupxmxxG8mpj2lYgttuw93OtLo/fJDLK0kgRHxnSxJOG1AYHMZ55PLPfW1D3MTXt7tzrs37r0PNylt4Nxvh36cvybsfAIusZA7YGMFgDnKg5yMd+e6tjjfROOK2NwH9EqpAzvqOOWP9a5t1xwpKTExZMJvo7wig4z7KOI9KJpY+pJ+bOCe1vkbjvx+FdJY+X6pJtbXHd9m14OjnywMz19q9nv/ACk35nX4I+9t5BP6at0nSCKNxEWJkOCVUZIBONTeAz9deejXGtqwYkSYGCzgbgEbgjFaL8SVrqMophRScBdzt2cMWzkc/q+rHIbcdDrQan7y9VZ7NHdedZhc15/wnjuFbCvJlh9JcEldsZOVyoGAQOVdGHpGQyBraU6mCqqyJqYnuXbA9pq4+9uFKcYumy5C586x8S4fLc2skSaQJmjwxkVSDFIrjGQc8jTW01vI80QhdTFbJd6+vJ1K4YgAacZGkZ/iFdXo++qCDUepeQF1QshbGc+G5AK58MiolK0Wt5VeN8Hle+iiYJCZl+b7Zccm1Zx9I7nw3prjoYwSR1uFJiBLhkKDb/ESQPrqxdI7VxecNmAZlWXq2wOWrTjl3bNScbu/7pfbY1Iw5+MnV1kWJJbKT9WaksCDeJKS1Wv0s88u7WSJtEqsjc8HwPIg948xtWHNWXp0yma2CkErbRhgDyJLHB8Dgiq3itqEtpWc/EjsTceRVlphUCpFYjOMKkVAphTETTioFMKBE0CgVIpiGAqRQKkUCJFMKKkUxEimFQBTCmIKYCgCnFAiBTAVAFOBToTI0+ZrG1sCc5Odt9s7csbbd/vPjWcVIFFcwTa3GnddhcmSQnYKAy5J7gMj/wBVPycnBaVz/mUg+QGN/bW2Y1PMD3VjNmurVls8ue3u5VDizJGca18jB8myNndu4Z0Abf5c+6lvpZAioqyS8yTgc/q+uukBRiskLg7T1F1rT3evXcdWz6SIbCG2MMqzpF1Mj9SNzgKBrJBxpA7u7lXGOfOstTiohhqF9upWPmJY2ztLdoc3iCyPE8en0sezY5rgNwqX1R7j8KuWKnFKWEm7HDNSgqSRSP7Ll9U/ZPwphweY934H4VdcUYpdSuZX66fJHAuLR2SJQpzGFxnxUYyK40vB7gsNhgZxsM75zv8AXV3xRppywU95Kzc1uOBYcOkVy5LAE6yuolQ2ckqO7ffG9dQ6g8bjnGSwHidvhW3poK1UIKKpGOeNKbuR3pLl4Uu51ifQ1nHbJl4sKAMBtYbLk5G2kcj9XD4d04lTQ8uovbiZY0EERWTWB1ZMmQ0WkjtYHaAHsrEYFPcPdSm1TwHurEsDtNl55t3R27Xprcy2YlaRYrr5QiMFjwOr7IOA+oAHvwc4zjfFbnSfjoJlEFwramVQnUy6goznVI3ZxkAgDx8q4ljcSQgiJ9AJDEADmOR3rFMS7M7nU7HUxPMk99CwPesc87cKW8wKxOSdydzT02mjFbBoWVQUwqKkVgNsZamoFMKYhhTilFMKYgFMBUU4oEApgKgUwpkkinApRTCgCRTCoFMBTESKagVIpkskCmoFFMRIpxSinFABTAUCgCgQAVIFTU0xBQKkVNAgAqaKmgCKKmigRFFTRQBFFTRQBFFTRQBFFTRQBFFTRQB//9k='
                            alt='...'
                        />
                    </div>

                </div>
            </div>

        )
    }
}

const mapStateToProps = state => ({
    local_variable: state
})

export default connect(mapStateToProps)(Electronics)