import React, { Component } from 'react'
import '../App.css'
import { connect } from 'react-redux'
import { addressSubmission, validateInput } from '../actions/validatorAction'
import { clickPay } from '../actions/validatorAction'
import { NavLink } from 'react-router-dom'
import { Redirect } from 'react-router-dom'

class CheckOut extends Component {

    render() {
        const cartProducts = this.props.local_variable.cart
        const totalAmount = cartProducts.reduce((acc, initial) => {
            return acc += Number(initial.price) * Number(initial.quantity)

        }, 0)

        console.log(this.props.address)
        const { nameErr, mobileErr, pincodeErr, addressErr, cityErr, stateErr, LocalityErr, isAddressSubmitted, cardErr, expiryErr, cvvErr, isPaymentSuccessful } = this.props.address


        return isPaymentSuccessful ? <Redirect to='/successfull' /> :

            (
                <div className='container-fluid checkoutBg margin-top'>
                    <div className='row d-flex justify-content-center'>

                        <div className='col col-md-8 mt-3'>

                            {/* {login} */}
                            <div className='d-flex bg-white p-2'>
                                <div className='border border-success text-center' style={{ width: '3.5%' }}>1</div>
                                <span className='ms-1'>LOGIN</span>
                            </div>

                            {/* Address */}
                            <div className='mt-2 bg-white'>
                                <div className='headerBg d-flex p-2'>
                                    <div className='border border-success bg-white text-primary text-center' style={{ width: '3.5%' }}>2</div>
                                    <span className='text-white ms-1 fw-bold'>DELIVERY ADDRESS</span>
                                </div>
                                <div>

                                    <form className='col p-3 lh-1'>

                                        <div className='d-flex'>
                                            <div className='d-flex flex-column'>
                                                <input className='form-control p-2' name='name' onBlur={this.props.validateInput} type='text' placeholder='Name' />
                                                <span className='text-danger ms-2 mt-1'>{nameErr}</span>
                                            </div>

                                            <div className='d-flex flex-column'>
                                                <input className='form-control p-2 ms-2' name='mobile' onBlur={this.props.validateInput} type="number" placeholder='10-digit mobile number' />
                                                <span className='text-danger ms-2 mt-1'>{mobileErr}</span>
                                            </div>


                                        </div>

                                        <div className='d-flex mt-3'>

                                            <div className='d-flex flex-column'>
                                                <input className='form-control p-2' name='pincode' onBlur={this.props.validateInput} type="text" placeholder='Pincode' />
                                                <span className='text-danger ms-2 mt-1'>{pincodeErr}</span>
                                            </div>
                                            <div className='d-flex flex-column'>
                                                <input className='form-control ms-2 p-2' name='locality' onBlur={this.props.validateInput} type="text" placeholder='Locality' />
                                                <span className='text-danger ms-2 mt-1'>{LocalityErr}</span>
                                            </div>
                                        </div>

                                        <div className='mt-3 d-flex flex-column'>
                                            <textarea className='form-control' name='address' onBlur={this.props.validateInput} placeholder='Address (Area and Street)' rows="4" cols="50" />
                                            <span className='text-danger ms-2 mt-1'>{addressErr}</span>

                                        </div>

                                        <div className='d-flex mt-3'>

                                            <div className='d-flex flex-column'>
                                                <input className='form-control p-2' name='city' onBlur={this.props.validateInput} type="text" placeholder='City/District/Town' />
                                                <span className='text-danger ms-2 mt-1'>{cityErr}</span>
                                            </div>
                                            <div className='d-flex flex-column'>
                                                <select className="form-control ms-2" name='state' onBlur={this.props.validateInput} id="inputState">
                                                    <option value="SelectState">Select State</option>
                                                    <option value="Andra Pradesh">Andra Pradesh</option>
                                                    <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                                    <option value="Assam">Assam</option>
                                                    <option value="Bihar">Bihar</option>
                                                    <option value="Chhattisgarh">Chhattisgarh</option>
                                                    <option value="Goa">Goa</option>
                                                    <option value="Gujarat">Gujarat</option>
                                                    <option value="Haryana">Haryana</option>
                                                    <option value="Himachal Pradesh">Himachal Pradesh</option>
                                                    <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                                                    <option value="Jharkhand">Jharkhand</option>
                                                    <option value="Karnataka">Karnataka</option>
                                                    <option value="Kerala">Kerala</option>
                                                    <option value="Madya Pradesh">Madya Pradesh</option>
                                                    <option value="Maharashtra">Maharashtra</option>
                                                    <option value="Manipur">Manipur</option>
                                                    <option value="Meghalaya">Meghalaya</option>
                                                    <option value="Mizoram">Mizoram</option>
                                                    <option value="Nagaland">Nagaland</option>
                                                    <option value="Orissa">Orissa</option>
                                                    <option value="Punjab">Punjab</option>
                                                    <option value="Rajasthan">Rajasthan</option>
                                                    <option value="Sikkim">Sikkim</option>
                                                    <option value="Tamil Nadu">Tamil Nadu</option>
                                                    <option value="Telangana">Telangana</option>
                                                    <option value="Tripura">Tripura</option>
                                                    <option value="Uttaranchal">Uttaranchal</option>
                                                    <option value="Uttar Pradesh">Uttar Pradesh</option>
                                                    <option value="West Bengal">West Bengal</option>
                                                    <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                                    <option value="Chandigarh">Chandigarh</option>
                                                    <option value="Dadar and Nagar Haveli">Dadar and Nagar Haveli</option>
                                                    <option value="Daman and Diu">Daman and Diu</option>
                                                    <option value="Delhi">Delhi</option>
                                                    <option value="Lakshadeep">Lakshadeep</option>
                                                    <option value="Pondicherry">Pondicherry</option>
                                                </select>
                                                <span className='text-danger ms-2 mt-1'>{stateErr}</span>
                                            </div>
                                        </div>

                                        <div className='d-flex mt-3'>

                                            <input className='form-control p-2' type="text" placeholder='Landmark (optional)' />
                                            <input className='form-control ms-2 p-2' type="text" placeholder='Alternative Phone (optional)' />

                                        </div>
                                        <div className='mt-2'>
                                            <span>Address type</span>
                                            <div className='d-flex justify-content-evenly mt-2'>
                                                <div className='d-flex align-items-center'>
                                                    <input type="radio" id="home" name="age" value="home" />
                                                    <label className='ms-2' htmlFor="home">Home <br />(All day delivery)</label>

                                                </div>
                                                <div className='d-flex align-items-center'>
                                                    <input type="radio" id="work" name="age" value="work" />
                                                    <label className='ms-2' htmlFor="work">Work <br /> (Delivery between 10 AM - 5 PM)</label>

                                                </div>
                                            </div>
                                        </div>

                                        {isAddressSubmitted ? <button onClick={this.props.addressSubmission} className='btn orangeBtn fw-bold mt-3 text-white'>Edit Address</button> :
                                            <button type='submit' className='btn orangeBtn fw-bold mt-3 text-white' onClick={this.props.addressSubmission}>SAVE AND DELIVER HERE</button>}


                                    </form>


                                </div>
                            </div>

                            {/* payment */}

                            <div className='container'>
                                <div className="row mt-3">
                                    <div className="d-flex align-items-baseline headerBg p-2">
                                        <div className='border border-success bg-white text-primary text-center' style={{ width: '3.5%' }}>3</div>
                                        <span className='fw-bold text-white ms-1'>PAYMENT OPTIONS</span>
                                    </div>
                                    <hr />
                                    <div>



                                        <div className='d-flex flex-column justify-content-between'>

                                            {/* Debit/Credit card */}

                                            <div className='d-flex flex-column'>
                                                <div className="mb-2">
                                                    <div
                                                        className="payment-container-buttons"
                                                        type="button"
                                                        data-bs-toggle="collapse"
                                                        data-bs-target="#button-credit-card"
                                                        aria-expanded="false"
                                                        aria-controls="button-credit-card"
                                                    >
                                                        <input htmlFor='card' type='radio' defaultChecked="checked" />
                                                        <label id='card' className='ms-2'> Credit/Debit Card </label>

                                                    </div>
                                                </div>
                                                <div className="collapse" id="button-credit-card">
                                                    <div className="card card-body">
                                                        <div>
                                                            <form className=''>
                                                                <div className="">
                                                                    <input type="text"
                                                                        name="card"
                                                                        onBlur={this.props.validateInput}
                                                                        className="payment-credit-input p-1"
                                                                        placeholder="Enter Card Number"
                                                                    /> <br />
                                                                    <span className='text-danger ms-2 mt-1'>{cardErr}</span>

                                                                </div>
                                                                <div className="">
                                                                    <div className='d-flex p-1'>
                                                                        Valid Thru
                                                                        <select className='ms-2' id="day" name="mm" onBlur={this.props.validateInput}>
                                                                            <option>MM</option>
                                                                            <option value="01">01</option>
                                                                            <option value="02">02</option>
                                                                            <option value="03">03</option>
                                                                            <option value="04">04</option>
                                                                            <option value="05">05</option>
                                                                            <option value="06">06</option>
                                                                            <option value="07">07</option>
                                                                            <option value="08">08</option>
                                                                            <option value="09">09</option>
                                                                            <option value="10">10</option>
                                                                            <option value="11">11</option>
                                                                            <option value="12">12</option>
                                                                            <option value="13">13</option>
                                                                            <option value="14">14</option>
                                                                            <option value="15">15</option>
                                                                            <option value="16">16</option>
                                                                            <option value="17">17</option>
                                                                            <option value="18">18</option>
                                                                            <option value="19">19</option>
                                                                            <option value="20">20</option>
                                                                            <option value="21">21</option>
                                                                            <option value="22">22</option>
                                                                            <option value="23">23</option>
                                                                            <option value="24">24</option>
                                                                            <option value="25">25</option>
                                                                            <option value="26">26</option>
                                                                            <option value="27">27</option>
                                                                            <option value="28">28</option>
                                                                            <option value="29">29</option>
                                                                            <option value="30">30</option>
                                                                            <option value="31">31</option>
                                                                        </select>
                                                                        <select className='ms-2' id="year" name="yyyy" onBlur={this.props.validateInput}>
                                                                            <option>YYYY</option>
                                                                            <option value="2020">2020</option>
                                                                            <option value="2021">2021</option>
                                                                            <option value="2022">2022</option>
                                                                            <option value="2023">2023</option>
                                                                            <option value="2024">2024</option>
                                                                            <option value="2025">2025</option>
                                                                            <option value="2026">2026</option>
                                                                            <option value="2027">2027</option>
                                                                            <option value="2028">2028</option>
                                                                            <option value="2029">2029</option>
                                                                            <option value="2030">2030</option>
                                                                            <option value="2031">2031</option>
                                                                            <option value="2032">2032</option>
                                                                            <option value="2033">2033</option>
                                                                            <option value="2034">2034</option>
                                                                            <option value="2035">2035</option>
                                                                        </select>

                                                                    </div>

                                                                    <span className='text-danger ms-2 mt-1'>{expiryErr}</span>
                                                                    <div>
                                                                        <input
                                                                            type="password"
                                                                            name="cvv"
                                                                            className="payment-credit-input p-1 w-25"
                                                                            placeholder="CVV"
                                                                            onBlur={this.props.validateInput}
                                                                        /> <br />
                                                                        <span className='text-danger ms-2 mt-1'>{cvvErr}</span>
                                                                    </div>
                                                                </div>
                                                                <div className="d-flex align-items-baseline">
                                                                    <input type="checkbox" id="check" />
                                                                    <label
                                                                        htmlFor="check"
                                                                        className="payments-para-styling ms-2"
                                                                    >
                                                                        Save this card for faster payments
                                                                    </label>

                                                                </div>
                                                                <div className="mt-4">

                                                                    <button className="place-order-button btn orangeBtn text-white fw-bold" onClick={this.props.clickPay}>
                                                                        PAY ₹{totalAmount.toFixed(2)}
                                                                    </button>

                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            {/* upi */}
                                            <div className='d-flex flex-column mt-2 mt-md-0'>
                                                <div className="mb-2">
                                                    <div
                                                        className="payment-container-buttons"
                                                        type="button"
                                                        data-bs-toggle="collapse"
                                                        data-bs-target="#button-upi"
                                                        aria-expanded="false"
                                                        aria-controls="button-upi"
                                                    >
                                                        <input htmlFor='upi' type='radio' defaultChecked="checked" />
                                                        <label id='upi' className='ms-2'>UPI</label>

                                                    </div>
                                                </div>
                                                <div className="collapse" id="button-upi">
                                                    <div className="card card-body">
                                                        <form>
                                                            <div className="mb-2">
                                                                <p>UPI</p>
                                                                <input
                                                                    type="text"
                                                                    className="payment-credit-input"
                                                                    placeholder="add @upi id"
                                                                />
                                                            </div>
                                                            <div>
                                                                <NavLink to="/orderplaced">
                                                                    <button
                                                                        onClick={this.handleUPIClick}
                                                                        className="place-order-button btn orangeBtn text-white fw-bold"
                                                                        disabled="true"
                                                                    >
                                                                        PAY ₹{totalAmount.toFixed(2)}
                                                                    </button>
                                                                </NavLink>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            {/* cod */}
                                            <div className='d-flex flex-column mt-2 mt-md-0'>
                                                <div className="mb-2">
                                                    <div
                                                        className="payment-container-buttons"
                                                        type="button"
                                                        data-bs-toggle="collapse"
                                                        data-bs-target="#button-cod"
                                                        aria-expanded="false"
                                                        aria-controls="button-cod"
                                                    >
                                                        <input htmlFor='cod' type='radio' defaultChecked="checked" />
                                                        <label id='cod' className='ms-2'>Cash On Delivery</label>

                                                    </div>
                                                </div>
                                                <div className="collapse" id="button-cod">
                                                    <div className="card card-body">

                                                        <NavLink to="/orderplaced">
                                                            <button className="place-order-button btn orangeBtn text-white fw-bold" disabled="true">
                                                                Place order
                                                            </button>
                                                        </NavLink>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div className='d-none d-md-block col-md-3 ms-1 mt-3'>
                            <div className='bg-white p-3'>
                                <h6 className='text-secondary mt-2'>PRICE DETAILS</h6>
                                <hr className='w-100' />
                                <div className='d-flex justify-content-between'>
                                    <span>Price ({cartProducts.length} items)</span>
                                    <span>₹{totalAmount.toFixed(2)}</span>
                                </div>
                                <div className='d-flex justify-content-between mt-4'>
                                    <span>Delivery Charges</span>
                                    <span className='text-success'>FREE</span>
                                </div>
                                <hr className='w-100' />
                                <div className='d-flex justify-content-between mt-4'>
                                    <p className='fw-bold'>Total Payable </p>
                                    <span className='fw-bold'>₹{totalAmount.toFixed(2)}</span>
                                </div>
                            </div>
                            <img className='mt-3' src='https://rukminim2.flixcart.com/lockin/300/300/images/promotion_banner_v2_active1.png?q=50' alt='...' />
                        </div>

                        <img className='d-md-none mt-2' src='https://rukminim2.flixcart.com/lockin/300/300/images/promotion_banner_v2_active1.png?q=50' alt='...' />


                    </div>
                </div>
            )
    }
}

const mapStateToProps = state => ({
    local_variable: state,
    address: state.deliveryAddress
})

export default connect(mapStateToProps, { addressSubmission, validateInput, clickPay })(CheckOut)