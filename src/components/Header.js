import React from 'react'
import { Link } from 'react-router-dom'
import '../App.css'
import { connect } from 'react-redux'
import { validateEmailorPhone } from '../actions/validatorAction'
import { validatePassword } from '../actions/validatorAction'
import { formSubmission } from '../actions/validatorAction'


function Header(props) {
    const loginBtn = props.local_variable.validator.isLogged ? "Logout" : 'Login'
    return (
        <div>
            <nav className="navbar navbar-expand-md navbar-light headerBg d-flex flex-column align-items-center">

                <div className="container-fluid">
                    <Link to='/' className='d-none d-md-block'>
                        <img src="https://static-assets-web.flixcart.com/batman-returns/batman-returns/p/images/fk-explorePlus-c5de64.png" alt='logo' className='cursor' style={{ width: "85px" }} />
                    </Link>

                    <div className="d-none d-md-flex align-items-center ms-3 bg-light">
                        <input className="form-control me-2 border-0" type="search" placeholder="Search" />
                        <i className="fa-solid fa-magnifying-glass me-3"></i>
                    </div>

                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon color-white"></span>
                    </button>

                    <Link to='/' className='d-md-none'>
                        <img src="https://static-assets-web.flixcart.com/batman-returns/batman-returns/p/images/fk-explorePlus-c5de64.png" alt='logo' className='cursor' style={{ width: "85px" }} />
                    </Link>

                    <div className='ms-md-5'>

                        <button type="button" className="btn bg-white nav-link text-primary cursor fw-bold" data-bs-toggle="modal" data-bs-target="#exampleModal">
                            {loginBtn}
                        </button>


                        <div className="modal fade" id="exampleModal" data-bs-backdrop="false" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div className="modal-dialog">
                                <div className="modal-content">
                                    <div className='d-flex justify-content-center'>

                                        <div className='loginContainer text-white bg-primary p-3 d-none d-sm-block'>

                                            <p className='fs-5'>Get access to your Orders, Wishlist and Recommendations</p>
                                        </div>

                                        <div>

                                            <div className="modal-header">
                                                <h5 className="modal-title fw-bold text-primary" id="exampleModalLabel">Login</h5>
                                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div className="modal-body">
                                                {props.local_variable.validator.isLogged ?
                                                    <div>
                                                        <h1 className='text-success'>Logged In Succesfully</h1>
                                                    </div>

                                                    :
                                                    <form onSubmit={props.formSubmission} noValidate="noValidate" style={{ width: "100%" }}>

                                                        <label htmlFor='firstName'>Enter Email/Mobile number</label> <br />
                                                        <input id='firstName' type='text' name='emailPhone' onBlur={props.validateEmailorPhone} /> <br />
                                                        <span className='text-danger'>{props.local_variable.validator.emailErrMessage}</span> <br />

                                                        <label htmlFor='current-password' className='mt-2'>Enter Password</label> <br />
                                                        <input id='current-password' type='password' onBlur={props.validatePassword} /> <br />
                                                        <p className='text-danger'>{props.local_variable.validator.passwordErrMessage}</p> <br />

                                                        <div className='d-flex justify-content-center mt-2'>
                                                            <p>OR</p>
                                                        </div>

                                                        <button style={{ width: "100%" }}>Request OTP</button>
                                                        <div className="modal-footer">
                                                            <button type="submit" className="btn orangeBtn text-white fw-bold">Submit</button>
                                                        </div>
                                                    </form>
                                                }

                                            </div>
                                            <div className="modal-footer">
                                                {props.local_variable.validator.isLogged ?
                                                    <Link to='/products'>
                                                        <button type="button" className="btn btn-warning" data-bs-dismiss="modal">Continue shopping</button>
                                                    </Link>
                                                    : null}
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                    <div className="collapse navbar-collapse ms-2 justify-content-end" id="navbarSupportedContent">
                        <ul className="d-flex flex-row justify-content-around navbar-nav mb-2 mb-lg-0">
                            <Link to='/'>
                                <li className="nav-item">
                                    <span className="nav-link active text-white cursor fw-bold">Home</span>
                                </li>
                            </Link>
                            <Link to='/clothes'>
                                <li className="nav-item">
                                    <span className="nav-link text-white cursor fw-bold">Clothing</span>
                                </li>
                            </Link>
                            <li className="nav-item">
                                <span className="nav-link text-white cursor fw-bold">Mobiles</span>
                            </li>
                            <Link to='/electronics'>
                                <li className="nav-item">
                                    <span className="nav-link text-white cursor fw-bold">Electronics</span>
                                </li>
                            </Link>
                            <Link to='/viewcart' className='d-none d-md-block'>
                                <li className="nav-item text-white fw-bold d-flex m-2">
                                    <i className="fa-solid fa-cart-shopping"><sup className='text-danger'>{props.local_variable.cart.length}</sup></i><span className='ms-1'>Cart</span>
                                </li>
                            </Link>
                        </ul>
                    </div>

                </div>

                <div className='d-flex align-items-center'>
                    <div className="d-flex d-md-none align-items-center m-2 bg-light w-100">
                        <input className="form-control me-2 border-0" type="search" placeholder="Search" />
                        <i className="fa-solid fa-magNamenifying-glass me-3"></i>
                    </div>
                    <Link to='/viewcart' className='d-flex d-md-none'>
                        <li className="nav-item text-white fw-bold d-flex m-2">
                            <i className="fa-solid fa-cart-shopping"><sup className='text-danger'>{props.local_variable.cart.length}</sup></i><span className='ms-1'>Cart</span>
                        </li>
                    </Link>
                </div>

            </nav>

        </div>
    )
}

const mapStateToProps = state => ({
    local_variable: state
})

export default connect(mapStateToProps, { validateEmailorPhone, validatePassword, formSubmission })(Header)