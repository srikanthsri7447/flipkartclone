import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import '../App.css'

class Jewelery extends Component {
    render() {
        const products = this.props.local_variable.products
        const jewelery = products.filter((eachProduct) => eachProduct.category === 'jewelery')
        return (
            <div className='container-fluid'>

                <div className='row'>

                    <div className='col col-md-9'>

                        <div className='d-flex justify-content-between'>
                            <h4>Jewelery</h4>
                            <Link to='/jewelery'>
                            <button className='btn btn-primary'>VIEW ALL</button>
                            </Link>
                        </div>

                        <hr className='w-100' />

                        <div className='d-flex overflow-auto example'>
                            {jewelery.map((eachProduct) => {

                                const { id, title, price, image } = eachProduct

                                return (

                                    <Link to={`/product/${id}`} key={id} className='d-flex flex-column align-items-center m-3 text-center' >
                                        <div className='imgSize'>
                                            <img src={image} alt='product' className='w-75' style={{ maxHeight: "100px" }} /> <br />
                                        </div>
                                        <span className='fw-bold textControl'>{title}</span> <br />
                                        <span className='text-success'>Price: ₹{price}</span>
                                    </Link>

                                )
                            })}
                        </div>

                    </div>

                    <div className='d-none d-md-block col-md-3'>
                        <img style={{width:'100%'}} src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS0x7SBT9-7a8Pg4-t-QIc8xqIAW77ZYfsmIg&usqp=CAU' alt='...' />
                    </div>
                </div>
            </div>

        )
    }
}
const mapStateToProps = state => ({
    local_variable: state
})

export default connect(mapStateToProps)(Jewelery)