import React from 'react'
import { connect } from 'react-redux'

function Successfull(props) {
    const addressDetails = props.local_variable.address
    const cartDetails = props.local_variable.cart

  return (
    <div className='container-fluid margin-top'>
        <div className='row'>
            <div className='col-12 headerBg p-2'>
                <h1 className='text-white fw-bold'>Order Placed Successfully</h1>
            </div>
            <div className='mt-2'>
                <h1>Hello {addressDetails.name}</h1>
                <p>We'll deliver your order as soon as possible via our delivery partner</p>
            </div>
            <div className='col-6'>
                <h4 className='orangeBtn p-2 fw-bold text-white'>Order Summary</h4>
                <div>
                    <h4>ordered Items : {cartDetails.length}</h4>
                    {cartDetails.map((eachItem)=>(
                        <div>{eachItem.title}</div>
                    ))}
                </div>
            </div>

        </div>

    </div>
  )
}

const mapStateToProps=state=>({
    local_variable : state
}
)
export default connect(mapStateToProps)(Successfull)