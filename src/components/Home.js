import React from 'react'
import Categories from './Categories';
import DealsOfTheDay from './DealsOfTheDay';
import Carousel from './Carousel';
import TrendingOffers from './TrendingOffers'
import TopMensClothing from './TopMensClothing'
import TopWomenClothing from './TopWomenClothing'
import Electronics from './Electronics'
import Jewelery from './Jewelery'
import * as Loader from 'react-loader-spinner'
import '../App.css'
import { connect } from 'react-redux';



function Home(props) {
  return !Object.keys(props.local_variable.products).length ? <div className='d-flex justify-content-center align-items-center vh-100'> <Loader.Audio /> </div> :    
  <div className='margin-top'> 
        <div>
          <Categories />
          <Carousel />
          <DealsOfTheDay />
          <TrendingOffers />
          <TopMensClothing />
          <TopWomenClothing />
          <Electronics />
          <Jewelery />
        </div>
    </div>
  
}

const mapStateToProps=state=>({
  local_variable : state
})

export default connect(mapStateToProps)(Home)