import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

class TrendingOffers extends Component {

  render() {
    const products = this.props.local_variable.products
    return (
      <div className='container-fluid mt-2'>
        <div className='row'>
          <div className='col'>

            <div className='d-flex justify-content-between'>
              <h4>Trending Offers</h4>
              <Link to='/products'><button className='btn btn-primary'>VIEW ALL</button> </Link>
            </div>

            <hr className='w-100' />

            <div className='d-flex overflow-auto example'>
              {products.map((eachProduct) => {
                const { id, title, price, image, category } = eachProduct

                return (
                  <Link to={`/product/${id}`} key={id} className='d-flex flex-column align-items-center m-3 text-center' >

                    <span>{category}</span> <br />
                    <div className='imgSize'>
                      <img src={image} alt='product' className='w-75' style={{ maxHeight: "100px" }} /> <br />
                    </div>
                    <span className='fw-bold textControl'>{title}</span> <br />
                    <span className='text-success'>Price: ₹{price}</span>

                  </Link>
                )
              })}
            </div>

          </div>
        </div>
      </div>

    )
  }
}

const mapStateToProps = state => ({
  local_variable: state
})

export default connect(mapStateToProps)(TrendingOffers);