import React from 'react'
import '../App.css'
import { Link } from 'react-router-dom'

function Categories() {
    return (

        <>
            <div className='d-flex d-md-none justify-content-between headerContainer overflow-auto'>
                
                <img className='w-25' alt='img' src='https://rukminim1.flixcart.com/flap/250/200/image/e3e79f5c3cbea9fd.jpg?q=90' />

                <img className='w-25' alt='img' src='https://rukminim1.flixcart.com/flap/250/200/image/0f3d008be60995d4.jpg?q=90' />

                <img className='w-25' alt='img' src='https://rukminim1.flixcart.com/flap/250/200/image/42f9a853f9181279.jpg?q=90' />

                <img className='w-25' alt='img' src='https://rukminim1.flixcart.com/flap/200/200/image/cbcb478744635781.jpg?q=90' />

                <img className='w-25' alt='img' src='https://rukminim1.flixcart.com/flap/200/200/image/913e96c334d04395.jpg?q=90' />

                <img className='w-25' alt='img' src='https://rukminim1.flixcart.com/flap/200/200/image/1faac897db7fa1e8.jpg?q=90' />

                <img className='w-25' alt='img' src='https://rukminim1.flixcart.com/flap/200/200/image/4be8a679014497f0.png?q=90' />

                <img className='w-25' alt='img' src='https://rukminim1.flixcart.com/flap/200/200/image/4da011879fdcb0ce.jpg?q=90' />

                <img className='w-25' alt='img' src='https://rukminim1.flixcart.com/flap/200/200/image/3e6d75f631ab6055.jpg?q=90' />

                <img className='w-25' alt='img' src='https://rukminim1.flixcart.com/flap/200/200/image/89d809684711712a.jpg?q=90' />

                <img className='w-25' alt='img' src='https://rukminim1.flixcart.com/flap/200/200/image/66e54a5fc733e2d7.jpg?q=90' />

                <img className='w-25' alt='img' src='https://rukminim1.flixcart.com/flap/200/200/image/aeb7da37a9e85209.jpg?q=90' />

                <img className='w-25' alt='img' src='https://rukminim1.flixcart.com/flap/200/200/image/ea3c3e826c117fea.jpg?q=90' />

                <img className='w-25' alt='img' src='https://rukminim1.flixcart.com/flap/200/200/image/356d37e9512c7fcb.jpg?q=90' />

                <img className='w-25' alt='img' src='https://rukminim1.flixcart.com/flap/200/200/image/efb218a1d2f237d6.jpg?q=90' />

                <img className='w-25' alt='img' src='https://rukminim1.flixcart.com/flap/200/200/image/222e7382a2c2bd72.jpg?q=90' />

                <img className='w-25' alt='img' src='https://rukminim1.flixcart.com/flap/200/200/image/973e7fd77daff493.jpg?q=90' />

            </div>

            <div className='d-none d-md-flex justify-content-between headerContainer overflow-auto'>

                <Link to='/products' className='d-flex flex-column justify-content-center align-items-center me-1 ms-1 w-50'>
                    <img className='w-75' alt='img' src='https://rukminim2.flixcart.com/flap/128/128/image/f15c02bfeb02d15d.png?q=100' />
                    <p>Top Offers</p>
                </Link>

                <div className='d-flex flex-column justify-content-center align-items-center me-1 ms-1 w-50'>
                    <img className='w-75' alt='img' src='https://rukminim2.flixcart.com/flap/128/128/image/29327f40e9c4d26b.png?q=75' />
                    <span>Grocery</span>
                </div>

                <Link to='/electronics' className='d-flex flex-column justify-content-center align-items-center me-1 ms-1 w-50'>
                    <img className='w-75' alt='img' src='https://rukminim2.flixcart.com/flap/128/128/image/22fddf3c7da4c4f4.png?q=75' />
                    <span>Mobiles</span>
                </Link>

                <Link to='/clothes' className='d-flex flex-column justify-content-center align-items-center me-1 ms-1 w-50'>
                    <img className='w-75' alt='img' src='https://rukminim2.flixcart.com/flap/128/128/image/c12afc017e6f24cb.png?q=75' />
                    <span>Fashion</span>
                </Link>

                <Link to='/electronics' className='d-flex flex-column justify-content-center align-items-center me-1 ms-1 w-50'>
                    <img className='w-75' alt='img' src='https://rukminim2.flixcart.com/flap/128/128/image/69c6589653afdb9a.png?q=75' />
                    <span>Electronics</span>
                </Link>

                <div className='d-flex flex-column justify-content-center align-items-center me-1 ms-1 w-50'>
                    <img className='w-75' alt='img' src='https://rukminim2.flixcart.com/flap/128/128/image/ab7e2b022a4587dd.jpg?q=75' />
                    <span>Home</span>
                </div>

                <Link to='/electronics' className='d-flex flex-column justify-content-center align-items-center me-1 ms-1 w-50'>
                    <img className='w-75' alt='img' src='https://rukminim2.flixcart.com/flap/128/128/image/0ff199d1bd27eb98.png?q=75' />
                    <span>Appliances</span>
                </Link>

                <div className='d-flex flex-column justify-content-center align-items-center me-1 ms-1 w-50'>
                    <img className='w-75' alt='img' src='https://rukminim2.flixcart.com/flap/128/128/image/71050627a56b4693.png?q=75' />
                    <span>Travel</span>
                </div>

                <div className='d-flex flex-column justify-content-center align-items-center me-1 ms-1 w-50'>
                    <img className='w-75' alt='img' src='https://rukminim2.flixcart.com/flap/128/128/image/dff3f7adcf3a90c6.png?q=75' />
                    <span >Beauty, Toys & More</span>
                </div>

            </div>
        </>

    )
}

export default Categories