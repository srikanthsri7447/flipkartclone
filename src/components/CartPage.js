import React, { Component } from 'react'
import '../App.css'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import removeCartAction from '../actions/removeCartAction'
import increaseQuantity from '../actions/increaseQuantityAction'
import decreaseQuantity from '../actions/decreaseQuantityAction'


class CartPage extends Component {

    render() {
        const products = this.props.local_variable.cart
        const totalAmount = products.reduce((acc,initial)=>{
            return acc += Number(initial.price) * Number(initial.quantity)
            
        },0)


        return products.length===0 ? (
        <div className='d-flex flex-column justify-content-center align-items-center cart-container'>
            <img src='https://rukminim2.flixcart.com/www/500/500/promos/16/05/2019/d438a32e-765a-4d8b-b4a6-520b560971e8.png?q=90' alt='...' />
            <h2>Your cart is empty!</h2>
            <p>It's a good day to buy the items you saved for later!</p>
            <Link to='/products'>
            <button className='btn btn-warning fs-2'>Continue Shopping</button>
            </Link>
        </div>) : (
            <div className='bg-container margin-top'>
                <div className='container'>
                    <div className='row d-flex justify-content-center'>
                        {products.map((eachProduct) => {
                            const { id, title, description, price, image, rating , quantity} = eachProduct
                            
                            return (

                                <div key={id} className='col-12 col-sm-6 col-md-3 d-flex flex-column align-items-center decCard m-2 text-center lh-sm' >

                                    <div className='imgSize'>
                                        <img src={image} alt='product' className='w-75' style={{ maxHeight: "100px" }} />
                                    </div>

                                    <div className='text-success'>
                                        <span>Rating: {rating.rate}</span>|<span>By: {rating.count}</span>
                                    </div>

                                    <span className='fw-bold textControl'>{title}</span> 
                                    <span className='descTextControl'>{description}</span> 
                                    <span className='text-danger fw-bold'>Price: ₹{price}</span> 
                                    <span>
                                        <button id={id} className='btn border-0 fs-2' onClick={this.props.decreaseQuantity}>-</button> <span>Qnt:{quantity} </span> <button id={id} className='btn border-0 fs-2' onClick={this.props.increaseQuantity}>+</button>
                                    </span>
                                    <div>
                                        <button className='btn btn-danger' id={id} onClick={this.props.removeCartAction}>Remove</button>
                                    </div>
                                </div>

                            )
                        })}
                    </div>

                </div>

                <div className='d-flex justify-content-evenly align-items-center fixed-bottom bg-white p-2'>
                    <p className='fw-bold'>Total amount: {totalAmount.toFixed(2)}</p>
                    <Link to='/checkOut'>
                    <button className='btn btn-warning ms-2'>Place Order</button>
                    </Link>
                </div>
            </div>
        )


    }
}

const mapStateToProps = state => ({
    local_variable: state
})

export default connect(mapStateToProps,{removeCartAction,increaseQuantity,decreaseQuantity})(CartPage)