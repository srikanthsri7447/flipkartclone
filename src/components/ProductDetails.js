import React, { Component } from 'react'
import productDetailsAction from '../actions/productDetailsAction'
import emptyProduct from '../actions/emptyProduct'
import { connect } from 'react-redux'
import * as Loader from 'react-loader-spinner'
import {addCartAction} from '../actions/updateCartAction'
import { Link } from 'react-router-dom'

class ProductDetails extends Component {
  componentDidMount() {
    const productId = this.props.match.params.id

    this.props.emptyProduct()
    this.props.productDetailsAction(productId)
  }

  onClickRec = (event) => {
    console.log(event)
    this.props.emptyProduct()
    this.props.productDetailsAction(event.target.id)
  }


  render() {

    const product = this.props.local_variable.product
    const { id, image, title, price, rating } = product
    const recomendedItems = this.props.local_variable.products.filter((eachProduct) => eachProduct.category === product.category)


    return Object.keys(this.props.local_variable.product).length ?
      (
        <div className='container'>

          <div className='row d-flex flex-column align-items-center margin-top'>

            <div className='container decCard d-flex col-12'>
              <div className='row d-flex align-items-center'>
                <img src={image} alt='product' className='col col-md-6 imageSize' />

                <div className='text-center col col-md-6'>
                  <h2 className='fw-bold'>{title}</h2>
                  {/* <span className='descTextControl'>{description}</span> <br /> */}
                  <div className='text-success'>
                    <span>Rating: {rating.rate}</span>|<span>By: {rating.count}</span>
                  </div>
                  <h4 className='text-danger fw-bold'>Price: ₹{price}</h4>
                  <button className='btn-primary m-2' id={id} onClick={this.props.addCartAction}>Add to Cart</button>

                </div>
              </div>

            </div>

            <div className='mt-3 col'>
              <h4>Recomended Items</h4>
              <div className='d-flex overflow-auto'>
                {recomendedItems.map((eachProduct) => {

                  return (
                    <div>
                      <Link to={`/product/${eachProduct.id}`} onClick={this.onClickRec} key={eachProduct.id} className='d-flex flex-column align-items-center  m-3 text-center' >
                        <div className=''>
                          <img id={eachProduct.id} src={eachProduct.image} alt='product' className='w-75' style={{ maxHeight: "100px" }} /> <br />
                        </div>
                        <span id={eachProduct.id} className='fw-bold textControl'>{eachProduct.title}</span> <br />
                        <span id={eachProduct.id} className='text-success'>Price: ₹{eachProduct.price}</span>
                      </Link>
                    </div>



                  )
                })}
              </div>
            </div>

          </div>


        </div>) : <div className='d-flex justify-content-center align-items-center vh-100'> <Loader.Audio /> </div>


  }
}

const mapStateToProps = state => ({
  local_variable: state
})

export default connect(mapStateToProps, { productDetailsAction, emptyProduct, addCartAction })(ProductDetails)