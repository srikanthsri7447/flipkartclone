import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { addCartAction } from '../actions/updateCartAction'
import '../App.css'

function JeweleryPage(props) {

    const products = props.local_variable.products
    const jewelery = products.filter((eachProduct) => eachProduct.category === 'jewelery' )

    return (
        <div className='margin-top bg-container'>
            <div className='container'>
                <div className='row d-flex justify-content-center'>
                    {jewelery.map((eachProduct) => {
                        const { id, title, description, price, image, rating } = eachProduct
                        return (

                            <div className='col-12 col-sm-6 col-md-3 d-flex flex-column justify-content-between align-items-center decCard m-2 text-center'>

                            <Link to={`/product/${id}`} key={id} className='d-flex flex-column justify-content-between align-items-center m-2 text-center' >
                                
                                <div className='imgSize'>
                                    <img src={image} alt='product' className='w-75' style={{ maxHeight: "100px" }} />
                                </div>

                                <div className='text-success'>
                                    <span>Rating: {rating.rate}| By: {rating.count}</span>
                                </div>

                                <span className='fw-bold textControl'>{title}</span> <br />
                                <span className='descTextControl'>{description}</span> <br />
                                <span className='text-danger fw-bold'>Price: ₹{price}</span>
                                </Link>
                                <div>
                                    <button className='btn btn-primary' id={id} onClick={props.addCartAction}>Add To Cart</button>
                                </div>
                            

                        </div>

                        )
                    })}
                </div>

            </div>
        </div>
    )
}


const mapStateToProps = state => ({
    local_variable: state
})

export default connect(mapStateToProps,{addCartAction})(JeweleryPage);
